use crate::errors::{HamurError, Result};
use crate::lexer::Token;
use std::iter::Peekable;

#[derive(Debug, PartialEq, Clone)]
pub enum Atom {
    Integer(i64),
    Float(f64),
    String(String),
    Symbol(String),
}

#[derive(Debug, PartialEq, Clone)]
pub enum SExpr {
    Atom(Atom),
    List(Vec<SExpr>),
}

impl ToString for SExpr {
    fn to_string(&self) -> String {
        match self {
            SExpr::Atom(Atom::Integer(i)) => i.to_string(),
            SExpr::Atom(Atom::Float(f)) => f.to_string(),
            SExpr::Atom(Atom::String(s)) => s.to_string(),
            SExpr::Atom(Atom::Symbol(s)) => s.to_string(),
            SExpr::List(list) => {
                let mut s = String::from("(");
                for expr in list {
                    s.push_str(&expr.to_string());
                    s.push(' ');
                }
                s.pop();
                s.push(')');
                s
            }
        }
    }
}

pub struct Parser {
    tokens: Peekable<std::vec::IntoIter<Token>>,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Parser {
            tokens: tokens.into_iter().peekable(),
        }
    }

    fn parse_atom(&mut self) -> Result<SExpr> {
        let atom = match self.tokens.next() {
            Some(Token::Integer(i)) => Atom::Integer(i),
            Some(Token::Float(f)) => Atom::Float(f),
            Some(Token::String(s)) => Atom::String(s),
            Some(Token::Symbol(s)) => Atom::Symbol(s),
            Some(t) => {
                return Err(HamurError::InvalidType(format!(
                    "Expected atom, got {:?}",
                    t
                )))
            }
            None => return Err(HamurError::EmptyToken),
        };

        Ok(SExpr::Atom(atom))
    }

    fn parse_expr(&mut self) -> Result<SExpr> {
        match self.tokens.peek() {
            Some(Token::LParen) => self.parse_list(),
            Some(_) => self.parse_atom(),
            None => Err(HamurError::EmptyToken),
        }
    }

    fn parse_list(&mut self) -> Result<SExpr> {
        self.tokens.next(); // consume open paren
        let mut exprs = Vec::new();

        while let Some(token) = self.tokens.peek() {
            if token == &Token::RParen {
                self.tokens.next();
                break;
            }
            let expr = self.parse_expr()?;
            exprs.push(expr);
        }

        Ok(SExpr::List(exprs))
    }

    pub fn parse(&mut self) -> Result<Vec<SExpr>> {
        let mut exprs = Vec::new();

        while let Some(_) = self.tokens.peek() {
            let expr = self.parse_expr()?;
            exprs.push(expr);
        }

        Ok(exprs)
    }
}

#[cfg(test)]
mod tests {
    use crate::lexer::Lexer;

    use super::*;

    #[test]
    fn test_parse_atom() {
        let mut parser = Parser::new(vec![Token::Integer(1)]);
        assert_eq!(parser.parse_atom(), Ok(SExpr::Atom(Atom::Integer(1))));
    }

    #[test]
    fn test_parse_expr() {
        let mut parser = Parser::new(vec![Token::Integer(1)]);
        assert_eq!(parser.parse_expr(), Ok(SExpr::Atom(Atom::Integer(1))));
    }

    #[test]
    fn test_parse_list() {
        let mut parser = Parser::new(vec![
            Token::LParen,
            Token::Integer(1),
            Token::Integer(2),
            Token::RParen,
        ]);
        assert_eq!(
            parser.parse_list(),
            Ok(SExpr::List(vec![
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ]))
        );
    }

    #[test]
    fn test_parse() {
        let mut parser = Parser::new(vec![
            Token::LParen,
            Token::Integer(1),
            Token::Integer(2),
            Token::RParen,
            Token::Integer(3),
        ]);
        assert_eq!(
            parser.parse(),
            Ok(vec![
                SExpr::List(vec![
                    SExpr::Atom(Atom::Integer(1)),
                    SExpr::Atom(Atom::Integer(2)),
                ]),
                SExpr::Atom(Atom::Integer(3)),
            ])
        );
    }

    #[test]
    fn parser_basic() {
        let tokens = vec![
            Token::LParen,
            Token::Symbol("define".to_string()),
            Token::LParen,
            Token::Symbol("factorial".to_string()),
            Token::Symbol("n".to_string()),
            Token::RParen,
            Token::LParen,
            Token::Symbol("if".to_string()),
            Token::LParen,
            Token::Symbol("=".to_string()),
            Token::Symbol("n".to_string()),
            Token::Integer(0),
            Token::RParen,
            Token::Integer(1),
            Token::LParen,
            Token::Symbol("*".to_string()),
            Token::Symbol("n".to_string()),
            Token::LParen,
            Token::Symbol("factorial".to_string()),
            Token::LParen,
            Token::Symbol("-".to_string()),
            Token::Symbol("n".to_string()),
            Token::Integer(1),
            Token::RParen,
            Token::RParen,
            Token::RParen,
            Token::RParen,
            Token::RParen,
        ];

        let mut parser = Parser::new(tokens);
        let ast = parser.parse().unwrap();

        assert_eq!(
            ast,
            vec![SExpr::List(vec![
                SExpr::Atom(Atom::Symbol("define".to_string())),
                SExpr::List(vec![
                    SExpr::Atom(Atom::Symbol("factorial".to_string())),
                    SExpr::Atom(Atom::Symbol("n".to_string()))
                ]),
                SExpr::List(vec![
                    SExpr::Atom(Atom::Symbol("if".to_string())),
                    SExpr::List(vec![
                        SExpr::Atom(Atom::Symbol("=".to_string())),
                        SExpr::Atom(Atom::Symbol("n".to_string())),
                        SExpr::Atom(Atom::Integer(0))
                    ]),
                    SExpr::Atom(Atom::Integer(1)),
                    SExpr::List(vec![
                        SExpr::Atom(Atom::Symbol("*".to_string())),
                        SExpr::Atom(Atom::Symbol("n".to_string())),
                        SExpr::List(vec![
                            SExpr::Atom(Atom::Symbol("factorial".to_string())),
                            SExpr::List(vec![
                                SExpr::Atom(Atom::Symbol("-".to_string())),
                                SExpr::Atom(Atom::Symbol("n".to_string())),
                                SExpr::Atom(Atom::Integer(1))
                            ])
                        ])
                    ])
                ])
            ])]
        );
    }

    #[test]
    fn test_parse_error() {
        let input = r#"(define (factorial n) (if (= n 0) 1 (* n (factorial (- n 1))))))"#;

        let lexer = Lexer::new(input);
        let tokens = lexer.collect::<Vec<_>>();
        let mut parser = Parser::new(tokens);

        assert!(parser.parse().is_err());
    }

    // Test parser by using the test file in ../tests/data/simple_program.clj
    #[test]
    fn test_with_simple_program() {
        let input = include_str!("../tests/data/simple_program.clj");
        let expected = vec![
            SExpr::List(vec![
                SExpr::Atom(Atom::Symbol("defn".to_string())),
                SExpr::Atom(Atom::Symbol("greet".to_string())),
                SExpr::Atom(Atom::Symbol("[name]".to_string())),
                SExpr::List(vec![
                    SExpr::Atom(Atom::Symbol("println".to_string())),
                    SExpr::List(vec![
                        SExpr::Atom(Atom::Symbol("str".to_string())),
                        SExpr::Atom(Atom::String("Hello, ".to_string())),
                        SExpr::Atom(Atom::Symbol("name".to_string())),
                        SExpr::Atom(Atom::String("!".to_string())),
                    ]),
                ]),
            ]),
            SExpr::List(vec![
                SExpr::Atom(Atom::Symbol("println".to_string())),
                SExpr::Atom(Atom::String("What is your name?".to_string())),
            ]),
        ];

        let lexer = Lexer::new(input);
        let tokens = lexer.collect::<Vec<_>>();
        let mut parser = Parser::new(tokens);

        assert_eq!(parser.parse(), Ok(expected));
    }
}
