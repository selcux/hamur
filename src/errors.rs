use std::{error::Error, fmt, result};

// type GenericError = Box<dyn std::error::Error + Send + Sync + 'static>;
// type GenericResult<T> = Result<T, GenericError>;

pub type Result<T> = result::Result<T, HamurError>;

#[derive(Debug, Clone, PartialEq)]
pub enum HamurError {
    InvalidType(String),
    EmptyToken,
    InvalidExpression(String),
    InvalidFunctionName(String),
}

impl fmt::Display for HamurError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            HamurError::InvalidType(s) => write!(f, "Invalid type: {}", s),
            HamurError::EmptyToken => write!(f, "Empty token"),
            HamurError::InvalidExpression(s) => write!(f, "Invalid expression: {}", s),
            HamurError::InvalidFunctionName(s) => write!(f, "Invalid function name: {}", s),
        }
    }
}

impl Error for HamurError {}
