use crate::errors::{HamurError, Result};
use crate::parser::{Atom, SExpr};

type FnName = String;

#[derive(Debug, Clone, PartialEq)]
pub enum Ast {
    Nil,
    Symbol(String),
    Keyword(String),
    Integer(i64),
    Float(f64),
    String(String),
    Boolean(bool),
    FnCall(FnName, Vec<Ast>),
    // TODO: add more types
}

fn try_parse_symbol(s: &String) -> Result<Ast> {
    match s.as_str() {
        "nil" => Ok(Ast::Nil),
        "true" => Ok(Ast::Boolean(true)),
        "false" => Ok(Ast::Boolean(false)),
        s if s.starts_with(":") => Ok(Ast::Keyword(s.into())),
        _ => Ok(Ast::Symbol(s.into())),
    }
}

fn try_parse_list(list: &Vec<SExpr>) -> Result<Ast> {
    if list.is_empty() {
        return Ok(Ast::Nil);
    }

    // Parse the function call as (fn args...)
    let mut args = list.into_iter();
    let fn_name = args.next().unwrap();
    let fn_name = match fn_name {
        SExpr::Atom(Atom::Symbol(s)) => s,
        _ => return Err(HamurError::InvalidFunctionName(fn_name.to_string())),
    };

    let fn_args = args
        .map(|arg| Ast::try_from(arg.clone()))
        .collect::<Result<Vec<Ast>>>()?;

    Ok(Ast::FnCall(fn_name.clone(), fn_args))
}

impl TryFrom<SExpr> for Ast {
    type Error = HamurError;

    fn try_from(value: SExpr) -> Result<Self> {
        match value {
            SExpr::Atom(Atom::Integer(i)) => Ok(Ast::Integer(i)),
            SExpr::Atom(Atom::Float(f)) => Ok(Ast::Float(f)),
            SExpr::Atom(Atom::String(s)) => Ok(Ast::String(s)),
            SExpr::Atom(Atom::Symbol(s)) => try_parse_symbol(&s),
            SExpr::List(list) => try_parse_list(&list),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_try_parse_symbol() {
        assert_eq!(try_parse_symbol(&"nil".into()).unwrap(), Ast::Nil);
        assert_eq!(
            try_parse_symbol(&"true".into()).unwrap(),
            Ast::Boolean(true)
        );
        assert_eq!(
            try_parse_symbol(&"false".into()).unwrap(),
            Ast::Boolean(false)
        );
        assert_eq!(
            try_parse_symbol(&":keyword".into()).unwrap(),
            Ast::Keyword(":keyword".into())
        );
        assert_eq!(
            try_parse_symbol(&"symbol".into()).unwrap(),
            Ast::Symbol("symbol".into())
        );
    }

    #[test]
    fn test_try_parse_list() {
        assert_eq!(
            try_parse_list(&vec![
                SExpr::Atom(Atom::Symbol("+".into())),
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ])
            .unwrap(),
            Ast::FnCall("+".into(), vec![Ast::Integer(1), Ast::Integer(2)],)
        );
    }

    #[test]
    fn test_try_from() {
        assert_eq!(
            Ast::try_from(SExpr::Atom(Atom::Integer(1))).unwrap(),
            Ast::Integer(1)
        );
        assert_eq!(
            Ast::try_from(SExpr::Atom(Atom::Float(1.0))).unwrap(),
            Ast::Float(1.0)
        );
        assert_eq!(
            Ast::try_from(SExpr::Atom(Atom::String("string".into()))).unwrap(),
            Ast::String("string".into())
        );
        assert_eq!(
            Ast::try_from(SExpr::Atom(Atom::Symbol("symbol".into()))).unwrap(),
            Ast::Symbol("symbol".into())
        );
        assert_eq!(Ast::try_from(SExpr::List(vec![])).unwrap(), Ast::Nil);
        assert_eq!(
            Ast::try_from(SExpr::List(vec![
                SExpr::Atom(Atom::Symbol("+".into())),
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2))
            ]))
            .unwrap(),
            Ast::FnCall("+".into(), vec![Ast::Integer(1), Ast::Integer(2)])
        );
    }

    #[test]
    fn test_try_from_error() {
        assert_eq!(
            Ast::try_from(SExpr::List(vec![
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ])),
            Err(HamurError::InvalidFunctionName("1".into()))
        );
    }

    #[test]
    fn test_try_from_list_error() {
        assert_eq!(
            Ast::try_from(SExpr::List(vec![
                SExpr::Atom(Atom::String("plus".into())),
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
                SExpr::Atom(Atom::Integer(3)),
            ])),
            Err(HamurError::InvalidFunctionName("plus".into()))
        );
    }
}
