/// Semantic analyzer for the language.
use crate::{errors::HamurError, parser::*};

/// Analyzes the given expressions and returns an error if any of them are invalid.
/// The analyzer will check for the following:
/// 1. The expression can be empty.
/// 2. The expression can be an empty list.
/// 3. The expression can be an atom.
/// 4.a. If the expression is the list, the first expression is a either a symbol (function) or a list.
/// 4.b. The function name is followed by a list of arguments.
pub fn analyze(expressions: &Vec<SExpr>) -> Result<(), Vec<HamurError>> {
    let mut errors: Vec<HamurError> = Vec::new();
    let mut iter = expressions.iter();

    while let Some(expr) = iter.next() {
        match expr {
            SExpr::List(list) => {
                if list.is_empty() {
                    continue;
                }

                if let Some(SExpr::Atom(atom)) = list.first() {
                    match atom {
                        Atom::Symbol(_) => {}
                        _ => {
                            errors.push(HamurError::InvalidExpression(format!(
                                "Expected a symbol, found {:?}",
                                atom
                            )));
                        }
                    }
                }

                let rest = &list[1..].to_vec();

                if let Err(errs) = analyze(rest) {
                    errors.append(&mut errs.clone());
                }
            }
            SExpr::Atom(_) => continue,
        }
    }

    if errors.is_empty() {
        Ok(())
    } else {
        Err(errors)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn analyze_empty() {
        let exprs = vec![];

        assert_eq!(analyze(&exprs), Ok(()));
    }

    #[test]
    fn analyze_empty_list() {
        let exprs = vec![SExpr::List(vec![])];

        assert_eq!(analyze(&exprs), Ok(()));
    }

    #[test]
    fn analyze_atom() {
        let exprs = vec![SExpr::Atom(Atom::Integer(1))];

        assert_eq!(analyze(&exprs), Ok(()));
    }

    #[test]
    fn analyze_list() {
        let exprs = vec![SExpr::List(vec![
            SExpr::Atom(Atom::Symbol("add".to_string())),
            SExpr::Atom(Atom::Integer(1)),
            SExpr::Atom(Atom::Integer(2)),
        ])];

        assert_eq!(analyze(&exprs), Ok(()));
    }

    #[test]
    fn analyze_list_nested() {
        let exprs = vec![SExpr::List(vec![
            SExpr::Atom(Atom::Symbol("add".to_string())),
            SExpr::List(vec![
                SExpr::Atom(Atom::Symbol("div".to_string())),
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ]),
            SExpr::Atom(Atom::Integer(3)),
        ])];
        assert_eq!(analyze(&exprs), Ok(()));
    }

    #[test]
    fn analyze_list_invalid() {
        let exprs = vec![SExpr::List(vec![
            SExpr::Atom(Atom::Integer(1)),
            SExpr::Atom(Atom::Integer(2)),
        ])];

        assert_eq!(
            analyze(&exprs),
            Err(vec![HamurError::InvalidExpression(
                "Expected a symbol, found Integer(1)".to_string()
            )])
        );
    }

    #[test]
    fn analyze_list_invalid_nested() {
        let exprs = vec![SExpr::List(vec![
            SExpr::Atom(Atom::Symbol("add".to_string())),
            SExpr::List(vec![
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ]),
            SExpr::Atom(Atom::Integer(3)),
        ])];
        assert_eq!(
            analyze(&exprs),
            Err(vec![HamurError::InvalidExpression(
                "Expected a symbol, found Integer(1)".to_string()
            )])
        );
    }

    #[test]
    fn analyze_list_invalid_nested_2() {
        let exprs = vec![SExpr::List(vec![
            SExpr::Atom(Atom::Symbol("add".to_string())),
            SExpr::List(vec![
                SExpr::Atom(Atom::Integer(1)),
                SExpr::Atom(Atom::Integer(2)),
            ]),
            SExpr::List(vec![
                SExpr::Atom(Atom::Integer(3)),
                SExpr::Atom(Atom::Integer(4)),
            ]),
        ])];

        assert_eq!(
            analyze(&exprs),
            Err(vec![
                HamurError::InvalidExpression("Expected a symbol, found Integer(1)".to_string()),
                HamurError::InvalidExpression("Expected a symbol, found Integer(3)".to_string())
            ])
        );
    }
}
