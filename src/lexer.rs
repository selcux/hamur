use std::iter::Peekable;
use std::str::Chars;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    LParen,
    RParen,
    String(String),
    Integer(i64),
    Float(f64),
    Symbol(String),
}

impl From<String> for Token {
    fn from(value: String) -> Self {
        match value.parse::<i64>() {
            Ok(integer) => Token::Integer(integer),
            Err(_) => match value.parse::<f64>() {
                Ok(float) => Token::Float(float),
                Err(_) => Token::Symbol(value),
            },
        }
    }
}

pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>,
}

impl<'a> Lexer<'a> {
    pub fn new(input: &'a str) -> Self {
        Lexer {
            input: input.chars().peekable(),
        }
    }

    fn parse_string(&mut self) -> Token {
        let mut buffer = String::new();

        while let Some(&ch) = self.input.peek() {
            match ch {
                '"' => {
                    self.input.next(); // consume the last '"'
                    break;
                }
                _ => {
                    buffer.push(ch);
                    self.input.next();
                }
            }
        }

        Token::String(buffer)
    }

    fn parse_token(&mut self, first_char: char) -> Option<String> {
        let mut buffer = first_char.to_string();

        while let Some(&ch1) = self.input.peek() {
            if ch1.is_whitespace() || ch1 == '(' || ch1 == ')' {
                break;
            }

            buffer.push(self.input.next().unwrap());
        }

        if buffer.is_empty() {
            None
        } else {
            Some(buffer)
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(ch) = self.input.next() {
            match ch {
                '(' => return Some(Token::LParen),
                ')' => return Some(Token::RParen),
                '"' => return Some(self.parse_string()),
                ch if ch.is_whitespace() => continue,
                _ => {
                    return match self.parse_token(ch) {
                        Some(token) => Some(token.into()),
                        None => None,
                    }
                }
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lexer_simple() {
        let input = "(+ 1 2)";
        let expected = vec![
            Token::LParen,
            Token::Symbol("+".to_string()),
            Token::Integer(1),
            Token::Integer(2),
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn convert_string_to_integer_token() {
        [("123", 123), ("-123", -123), ("+123", 123)]
            .iter()
            .for_each(|&(s, i)| {
                let token: Token = s.to_string().into();
                assert_eq!(token, Token::Integer(i));
            });
    }

    #[test]
    fn convert_string_to_float_token() {
        [
            ("123.0", 123.0),
            ("-123.0", -123.0),
            ("+123.0", 123.0),
            ("123.456", 123.456),
            ("-123.456", -123.456),
            ("+123.456", 123.456),
            ("123.0e2", 123.0e2),
            ("-123.0e2", -123.0e2),
            ("+123.0e2", 123.0e2),
            ("123.456e2", 123.456e2),
            ("-123.456e2", -123.456e2),
            ("+123.456e2", 123.456e2),
            ("123.0e-2", 123.0e-2),
            ("-123.0e-2", -123.0e-2),
            ("+123.0e-2", 123.0e-2),
            ("123.456e-2", 123.456e-2),
            ("-123.456e-2", -123.456e-2),
            ("+123.456e-2", 123.456e-2),
        ]
        .iter()
        .for_each(|&(s, f)| {
            let token: Token = s.to_string().into();
            assert_eq!(token, Token::Float(f));
        });
    }

    #[test]
    fn convert_string_to_symbol() {
        [
            "+", "-", "*", "/", ">", "<", "=", "and", "or", "not", "if", "cond", "lambda", "define",
        ]
        .iter()
        .for_each(|&s| {
            let token: Token = s.to_string().into();
            assert_eq!(token, Token::Symbol(s.to_string()));
        });
    }

    #[test]
    fn lexer_complex() {
        let input = "(+ 1 2 (- 3 4) (* 5 6) (/ 7 8))";
        let expected = vec![
            Token::LParen,
            Token::Symbol("+".to_string()),
            Token::Integer(1),
            Token::Integer(2),
            Token::LParen,
            Token::Symbol("-".to_string()),
            Token::Integer(3),
            Token::Integer(4),
            Token::RParen,
            Token::LParen,
            Token::Symbol("*".to_string()),
            Token::Integer(5),
            Token::Integer(6),
            Token::RParen,
            Token::LParen,
            Token::Symbol("/".to_string()),
            Token::Integer(7),
            Token::Integer(8),
            Token::RParen,
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lexer_nested_complex() {
        let input = "(+ 1 2 (- 3 4) (* 5 6) (/ 7 8 (+ 9 10)))";
        let expected = vec![
            Token::LParen,
            Token::Symbol("+".to_string()),
            Token::Integer(1),
            Token::Integer(2),
            Token::LParen,
            Token::Symbol("-".to_string()),
            Token::Integer(3),
            Token::Integer(4),
            Token::RParen,
            Token::LParen,
            Token::Symbol("*".to_string()),
            Token::Integer(5),
            Token::Integer(6),
            Token::RParen,
            Token::LParen,
            Token::Symbol("/".to_string()),
            Token::Integer(7),
            Token::Integer(8),
            Token::LParen,
            Token::Symbol("+".to_string()),
            Token::Integer(9),
            Token::Integer(10),
            Token::RParen,
            Token::RParen,
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn basic_list() {
        let input = r#"(define (factorial n) (if (= n 0) 1 (* n (factorial (- n 1)))))"#;
        let expected = vec![
            Token::LParen,
            Token::Symbol("define".to_string()),
            Token::LParen,
            Token::Symbol("factorial".to_string()),
            Token::Symbol("n".to_string()),
            Token::RParen,
            Token::LParen,
            Token::Symbol("if".to_string()),
            Token::LParen,
            Token::Symbol("=".to_string()),
            Token::Symbol("n".to_string()),
            Token::Integer(0),
            Token::RParen,
            Token::Integer(1),
            Token::LParen,
            Token::Symbol("*".to_string()),
            Token::Symbol("n".to_string()),
            Token::LParen,
            Token::Symbol("factorial".to_string()),
            Token::LParen,
            Token::Symbol("-".to_string()),
            Token::Symbol("n".to_string()),
            Token::Integer(1),
            Token::RParen,
            Token::RParen,
            Token::RParen,
            Token::RParen,
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lexer_empty() {
        let input = "";
        let expected = vec![];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lexer_open_paren() {
        let input = "(";
        let expected = vec![Token::LParen];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lexer_string_literal() {
        let input = "(str \"Hello World\" 2)";
        let expected = vec![
            Token::LParen,
            Token::Symbol("str".to_string()),
            Token::String("Hello World".to_string()),
            Token::Integer(2),
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }

    // Test lexer by using the test file in ../tests/data/simple_program.clj
    #[test]
    fn test_with_simple_program() {
        let input = include_str!("../tests/data/simple_program.clj");
        let expected = vec![
            Token::LParen,
            Token::Symbol("defn".to_string()),
            Token::Symbol("greet".to_string()),
            Token::Symbol("[name]".to_string()),
            Token::LParen,
            Token::Symbol("println".to_string()),
            Token::LParen,
            Token::Symbol("str".to_string()),
            Token::String("Hello, ".to_string()),
            Token::Symbol("name".to_string()),
            Token::String("!".to_string()),
            Token::RParen,
            Token::RParen,
            Token::RParen,
            Token::LParen,
            Token::Symbol("println".to_string()),
            Token::String("What is your name?".to_string()),
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let tokens: Vec<Token> = lexer.collect();

        assert_eq!(tokens, expected);
    }
}
