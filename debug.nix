{pkgs ? import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {}}:

pkgs.mkShell {
  name = "debug-env";

  shellHook = ''
  '';

  RUST_LOG="debug";
}
